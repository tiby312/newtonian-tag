package com.reed.newtoniantag;

import java.io.Serializable;

class Holderable extends Target implements Serializable{	
	private static final long serialVersionUID = 1L;
	Holderable(float x,float y){
		super(x,y);
	}
	private int pointerHolderID=-1;
	private boolean beingHeld=false;
	void capture(int pointerHolderID){
		this.pointerHolderID=pointerHolderID;
		beingHeld=true;
	}
	int holderID(){
		return pointerHolderID;
	}
	void release(){
		beingHeld=false;
		this.pointerHolderID=-1;
	}
	boolean beingHeld(){
		return beingHeld;
	}
	
}

public class Beacon extends Holderable implements Serializable{
	private static final long serialVersionUID = 1L;
	static float radius=0.1f;
	static float diam=1.0f;
	//private Target target;
	//Vec2 pos;
	Beacon(float x, float y) {
		super(x,y);
		//pos=new Vec2(x,y);	
		//target=new Target(x,y);
	}
	
	void setTarget(float x,float y){
		//target.pos.x=x;
		//target.pos.y=y;
		pos.x=x;
		pos.y=y;
	}
	void step(){
        //answer=(target.pos+target.vel*timeNeededToStop)-(pos+vel*timeNeededToStop);
		//pos.copy(target.pos);
//		Vec2 j=new Vec2(target.pos);
//		j.sub(pos);
//		j.mult(0.4f);
//		pos.add(j);
			
	}
	
		
}
