package com.reed.newtoniantag;

import java.io.Serializable;

import android.graphics.Color;
import android.util.FloatMath;

class Circle implements Serializable {
	private static final long serialVersionUID = 1L;
	Vec2 pos;
	float radius;

	Circle(float x, float y) {
		pos = new Vec2(x, y);
		radius = 0.3f;
	}
}

class Target extends Circle implements Serializable {
	private static final long serialVersionUID = 1L;
	Vec2 vel;

	Target(float x, float y) {
		super(x, y);
		vel = new Vec2();
	}

	void draw() {

		MGameS.drawCircle(pos.x, pos.y, 0.1f, Color.GREEN);

		// p.setColor(Color.YELLOW);
		// String str="ang="+ang+ "pos="+pos+" angl="+ang.lengthSqr();
		// g.drawText(c,str, pos, p);

	}
}

public class Bot extends Ship implements Serializable {

	private static final long serialVersionUID = 1L;

	// Target target;

	static class AvoidAnswer implements Serializable {
		private static final long serialVersionUID = 1L;
		Vec2 v1;
		Vec2 v2;

		AvoidAnswer() {
			v1 = new Vec2();
			v2 = new Vec2();
		}
	}

	int team;
	int teamcooldown = 0;
	boolean controlledByPlayer = false;
	Player owner;
	// static Target dummyTarg=new Target(0,0);
	final float maxsteerspeed;
	Vec2 desiredSteer;

	static Vec2 tempp = new Vec2();
	static Vec2 tempp2 = new Vec2();
	static Vec2 tempp3 = new Vec2();
	
	
	static final float desiredDis = 4.0f;
	static final float desiredDisSqr = desiredDis * desiredDis;
	
	Bot(float x, float y, int color) {
		super(x, y);
		// target=null;
		this.team = color;
		// target.copy(pos);
		// target.pos.x=1.0f;
		// target.pos.y=1.0f;
		maxsteerspeed = 10.0f;
		desiredSteer = new Vec2();
	}

	void draw() {

		MGameS.drawCircle(pos.x, pos.y, radius, team);
		// Vec2 ja=new Vec2(ang);

		if (teamcooldown > 0) {
			MGameS.drawCircle(pos.x, pos.y, radius, Color.BLACK);
		}
		tempp.copy(ang);
		tempp.mult(radius);
		tempp.add(pos);

		MGameS.drawLine(pos, tempp, Color.WHITE);
		{
			// Vec2 a=new Vec2(pos);
			tempp.copy(pos);
			tempp2.copy(ang);
			// Vec2 j=new Vec2(ang);
			tempp2.neg();
			tempp2.mult(radius);
			tempp.add(tempp2);
			MGameS.drawLine(pos, tempp, Color.RED, goingforward);
		}
		{
			// Vec2 a=new Vec2(pos);
			// Vec2 j=new Vec2(ang);
			tempp.copy(pos);
			tempp2.copy(ang);
			tempp2.perp90deg();
			tempp2.mult(radius);
			tempp.add(tempp2);
			MGameS.drawLine(pos, tempp, Color.RED, goingleft);
		}
		{
			tempp.copy(pos);
			tempp2.copy(ang);
			tempp2.neg();
			tempp2.perp90deg();
			tempp2.mult(radius);
			tempp.add(tempp2);
			MGameS.drawLine(pos, tempp, Color.RED, goingright);

		}

	}

	void step() {
		super.step();
		// goToTarget();

		teamcooldown--;
		// j.copy(target);
		// j.sub(pos);
		// turnTo(j);
		// turnToAndGoForward(j);

	}

	void reset() {
		desiredSteer.x = 0;
		desiredSteer.y = 0;
	}

	float timeToRot() {
		return rottime + (speed / linearacc);

	}

	void getPredPos(Vec2 p) {
		float k = timeToRot();
		p.x = pos.x + vel.x * k;
		p.y = pos.y + vel.y * k;
	}

	void arrive(Target t) {
		// Vec2 a=new Vec2();
		arrive(this.timeToRot(), tempp, t);
		this.desiredSteer.add(tempp);
	}

	private void arrive(float timeNeededToStop, Vec2 answer, Target target) {
		// TODO
		// Num
		// timeNeededToStop=(b.grottime+(b.Speed())/b.Lacc());//+timeNeededToRot;

		// Num timeNeededToStop=b.getTimeNeededToStopRot();
		// cout<<target.pos<<endl;
		// maxtimespeed=
		// b.timeNeededTo

		// return Vec2(10.0f,0);

		// answer=(target.pos+target.vel*timeNeededToStop)-(pos+vel*timeNeededToStop);
		// Vec2 temp=new Vec2();
		answer.x = (target.pos.x + target.vel.x * timeNeededToStop)
				- (pos.x + vel.x * timeNeededToStop);
		answer.y = (target.pos.y + target.vel.y * timeNeededToStop)
				- (pos.y + vel.y * timeNeededToStop);

		// Vec2 finalVel=targetOffset;
		// Num maxspeed=0.5f;

		// if(Speed()!=0){
		// finalVel.truncate(b.MaxSteerSpeed());
		// finalVel.truncate(b.)
		answer.truncate(maxsteerspeed);
		answer.sub(vel);
		// answer.copy(tempp);

		// return finalVel;

	}

	void turnToAndGoForward(Vec2 desiredacc) {
		float innerangle = turnTo(desiredacc);

		float accuracy = (float) Tools.eighthpi;
		if (innerangle <= accuracy) {
			goforward();
		} else {
		}
	}

	float turnTo(Vec2 p) {
		float desiredaccLengthSqr = p.lengthSqr();
		float desiredaccLength = (float) Math.sqrt(desiredaccLengthSqr);

		Vec2 desiredaccUnitVector = tempp;// new Vec2();
		desiredaccUnitVector.copy(p);
		desiredaccUnitVector.div(desiredaccLength);
		// cout<<desiredaccUnitVector<<endl;

		Vec2 rotUnitVector = ang;// Vec2(cos(Ang()),sin(Ang()));

		// Find magnitude of inner angle using dot product
		// Vec2 rotUnitVector=s.t();
		float innerProduct = Vec2.innerProduct(rotUnitVector,
				desiredaccUnitVector);// rotUnitVector.x*desiredaccUnitVector.x+rotUnitVector.y*desiredaccUnitVector.y;
		float innerAngle = Vec2.innerAngle(innerProduct);

		// cout<<rotUnitVector<<","<<desiredaccUnitVector<<","<<innerAngle<<endl;

		// use crossproduct to find right sign.
		// http://math.stackexchange.com/questions/45097/dot-product-negative-angle
		float crossProductZ = Vec2.crossProduct(desiredaccUnitVector,
				rotUnitVector);// ((desiredaccUnitVector.x*rotUnitVector.y)-(desiredaccUnitVector.y*rotUnitVector.x));
		// the angle between targetmove angle and rotation with correct sign
		// cout<<crossProductZ<<endl;

		float targetMoveAngleRelative;
		if (crossProductZ == 0.0f && innerAngle > Tools.halfPI) {
			targetMoveAngleRelative = innerAngle * 1.0f;
		} else {
			targetMoveAngleRelative = innerAngle * Tools.sign(crossProductZ);
		}

		// Calculate the angle needed to come to a stop
		float angleNeededToStopRotate = ((FloatMath.pow(Math.abs(angv), 2) / (2.0f * (turnacc))) * Tools
				.sign(angv));

		// Add that to the target angle
		float targetMoveAngleRelativeCompensated = targetMoveAngleRelative
				+ angleNeededToStopRotate;

		if (targetMoveAngleRelativeCompensated > 0) {
			goright();
		}
		if (targetMoveAngleRelativeCompensated < 0) {
			goleft();

		}
		return innerAngle;

	}

	static boolean separate(Bot a, Bot b, AvoidAnswer ans) {
		Vec2 apredpos = tempp;
		a.getPredPos(apredpos);

		Vec2 bpredpos = tempp2;
		b.getPredPos(bpredpos);

		Vec2 offset = tempp3;
		offset.copy(bpredpos);
		offset.sub(apredpos);
		
		float ls = offset.lengthSqr();

		if (ls < 0.001) {
			return false;
		}

		if (ls < desiredDisSqr) {
			float ll = (float) Math.sqrt(ls);
			float diff = 1.0f - (ll / desiredDis);

			// int alpha=(int)(diff*255);
			// MGameS.drawLine( apredpos, bpredpos, Color.MAGENTA,alpha);

			Vec2 normedOffset = tempp;
			normedOffset.copy(offset);
			normedOffset.div(ls);

			Vec2 jj = tempp2;// new Vec2(aa);
			jj.copy(normedOffset);
			jj.mult(diff);
			ans.v2.copy(jj);
			jj.neg();
			ans.v1.copy(jj);

			return true;
		}
		return false;
	}

	static boolean avoid(Bot a, Bot b, AvoidAnswer ans) {
		Vec2 offset = tempp;
		offset.copy(b.pos);// new Vec2(b.pos);
		offset.sub(a.pos);

		float ls=offset.lengthSqr();
		if(ls>=Bot.desiredDisSqr){
			return false;
		}
		float ll = (float) Math.sqrt(ls);

		Vec2 offsetnorm = tempp2;
		offsetnorm.copy(offset);// new Vec2(offset);
		offsetnorm.div(ll);// normalize(offset);

		Vec2 relativeVNorm = tempp3;// new Vec2(b.vel);
		relativeVNorm.copy(b.vel);
		relativeVNorm.sub(a.vel);
		relativeVNorm.normalize();
		float relativeVSqr = relativeVNorm.lengthSqr();
		float relativeVMag = (float) Math.sqrt(relativeVSqr);
		// Vec2 relativeVNorm=new Vec2(relativeV);
		relativeVNorm.div(relativeVMag);

		float d = Vec2.innerProduct(offset, relativeVNorm);

		float h = (float) Math.sqrt(ll * ll - d * d);

		float t = -d / relativeVMag;// not sure why i have to negate it

		float radiusTot = a.radius + b.radius;
		float leeway = radiusTot / 2.0f;
		float radlee = radiusTot + leeway;

		// float rottimetotal=Math.max(a.timeToRot(),b.timeToRot())/20.0f;
		float rottimetotal = Math.max(a.rottime, b.rottime);
		t -= rottimetotal;
		// t=Math.max(0.0f,t);

		final float maxtime = 40.0f;
		if (t >= 0.0f && t < maxtime && h < radlee) {

			float timeurgency = 1.0f - (t / maxtime);
			float hurgency = 1.0f - (h / radlee);

			float urgencyTotal = Math.max(timeurgency, hurgency);// )/2.0f;

			// int alpha=(int)(urgencyTotal*255);
			// MGameS.drawLine(a.pos, b.pos, Color.CYAN,alpha);

			// use crossproduct to find right sign.
			// http://math.stackexchange.com/questions/45097/dot-product-negative-angle

			float crossProductZ = Vec2.crossProduct(offsetnorm, relativeVNorm);
			// the angle between targetmove angle and rotation with correct sign
			// float targetMoveAngleRelative=innerAngle*

			Vec2 targAng = tempp;
			targAng.set(relativeVNorm.y, -relativeVNorm.x);// -(relativeVNorm);

			Vec2 j = tempp2;// new Vec2();
			j.copy(targAng);
			j.mult(-urgencyTotal * Tools.sign(crossProductZ));
			ans.v1.copy(j);
			j.neg();
			ans.v2.copy(j);
			return true;
		}
		return false;
	}

}