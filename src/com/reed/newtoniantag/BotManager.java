package com.reed.newtoniantag;

import java.io.Serializable;
import java.util.ArrayList;


interface CollisionListener{
	void onCollision(Bot a,Bot b);
}
class CollisionEvent implements Serializable{
	private static final long serialVersionUID = 1L;
	ArrayList<CollisionListener> listeners;
	CollisionEvent(){
		listeners=new ArrayList<CollisionListener>();
	}
	void register(CollisionListener e){
		listeners.add(e);
	}
	void fire(Bot a,Bot b){
		for(CollisionListener i:listeners){
			i.onCollision(a,b);
		}
	}
	void clear(){
		listeners.clear();
	}
}


public class BotManager implements Serializable{	
	private static final long serialVersionUID = 1L;
	private ArrayList<Bot> bots=new ArrayList<Bot>();
	private CollisionEvent event=new CollisionEvent();
	
	Moveable.Answer colans=new Moveable.Answer();
	void register(CollisionListener e){
		event.register(e);
	}
	//private Bot botWhoIsIt;
	Bot createBot(float x,float y,int col){
		Bot j=new Bot(x,y,col);
		bots.add(j);		
		return j;
	}
	
	void draw(){
		for(Bot i:bots){
			i.draw();
		}
	}
	void step(){
			
		for(int i=0;i<bots.size();i++){
			for(int j=i+1;j<bots.size();j++){				
				Bot aa=bots.get(i);
				Bot bb=bots.get(j);			
				if(Bot.checkCollision(aa,bb, colans)){					
					Bot.handleCollide(aa,bb, colans);
					event.fire(aa,bb);
				}				
			}
		}
		
		

	}
	
	void constrain(RectFS bounds){		
		for(int i=0;i<bots.size();i++){
			constrain(bots.get(i),bounds);
		}
	}
	
	void move(){		
		for(int i=0;i<bots.size();i++){		
			Bot j=bots.get(i);
			j.step();			
			j.turnToAndGoForward(j.desiredSteer);			
			j.reset();
			
		}
	}
	private void constrain(Moveable m,RectFS bounds){
		if(!Tools.contains(m, bounds)){
			float damp=0.5f;
			if(m.pos.x-m.radius<bounds.left){
				m.pos.x=bounds.left+m.radius;
				m.vel.x=-m.vel.x*damp;
			}else if(m.pos.x+m.radius>bounds.right){
				m.pos.x=bounds.right-m.radius;
				m.vel.x=-m.vel.x*damp;			
			}
			if(m.pos.y-m.radius<bounds.top){
				m.pos.y=bounds.top+m.radius;
				m.vel.y=-m.vel.y*damp;
			}else if(m.pos.y+m.radius>bounds.bottom){
				m.pos.y=bounds.bottom-m.radius;
				m.vel.y=-m.vel.y*damp;
			}
		}
	}
}
