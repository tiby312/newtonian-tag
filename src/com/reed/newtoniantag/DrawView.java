package com.reed.newtoniantag;


import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceView;


//interface DoubleTapListener{
//	void onDoubleTap(MotionEvent event);
//}
//class DoubleTapEvent{
//	ArrayList<DoubleTapListener> listeners;
//	DoubleTapEvent(){
//		listeners=new ArrayList<DoubleTapListener>();
//	}
//	void register(DoubleTapListener e){
//		listeners.add(e);
//	}
//	void fire(MotionEvent e){
//		for(DoubleTapListener i:listeners){
//			i.onDoubleTap(e);
//		}
//	}
//	void clear(){
//		listeners.clear();
//	}
//}
interface MouseEventListener{
	void onClick(MotionEvent event);
}
class MouseEvent{
	ArrayList<MouseEventListener> listeners;
	MouseEvent(){
		listeners=new ArrayList<MouseEventListener>();
	}
	void register(MouseEventListener e){
		listeners.add(e);
	}
	void fire(MotionEvent e){
		for(MouseEventListener i:listeners){
			i.onClick(e);
		}
	}
	void clear(){
		listeners.clear();
	}
}
interface ReadyEventListener{
	void onReady();
}
class ReadyEvent{
	ArrayList<ReadyEventListener> listeners;
	ReadyEvent(){
		listeners=new ArrayList<ReadyEventListener>();
	}
	void register(ReadyEventListener e){
		listeners.add(e);
	}
	void fire(){
		for(ReadyEventListener i:listeners){
			i.onReady();
		}
	}
}
//SurfaceHolder.Callback
class DrawView extends SurfaceView{

	public DrawView(Context context, AttributeSet attrs) {
	    super(context, attrs);
	    init(context);	    
	}

	public DrawView(Context context) {
		super(context);		
		init(context);			
	}
	public DrawView(Context context, AttributeSet attrs, int defStyle) {
	    super(context, attrs, defStyle);
	    init(context);
	    // TODO Auto-generated constructor stub
	}
	public void init(Context context){
		MGameS.sh=getHolder();
		MGameS.dv=this;		        
	}

	public void onMeasure(int widthMeasureSpec,int heightMeasureSpec){
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
	    int widthSize = MeasureSpec.getSize(widthMeasureSpec);
	    int heightMode = MeasureSpec.getMode(heightMeasureSpec);
	    int heightSize = MeasureSpec.getSize(heightMeasureSpec);

	    
		
	    
	    
	    int ax=MGame.aspectRatioX;
	    int ay=MGame.aspectRatioY;
	    //Log.i("j",widthSize+"asdfa");
	    int desiredWidth=widthSize;
		int desiredHeight=(int)(widthSize/(float)(ax/(float)ay));
		
		if(desiredHeight>heightSize){
			float off=heightSize/(float)desiredHeight;
			desiredHeight=(int)(desiredHeight*off);
			desiredWidth=(int)(desiredWidth*off);
		}
		//Log.i("desiredWidth",desiredWidth+","+desiredHeight);
	    
	    
	    
	    int width;
	    int height;

	    //Measure Width
	    if (widthMode == MeasureSpec.EXACTLY) {
	        //Must be this size
	    	
	        width = widthSize;
	    } else if (widthMode == MeasureSpec.AT_MOST) {
	        //Can't be bigger than...
	        width = Math.min(desiredWidth, widthSize);	        
	    } else {
	        //Be whatever you want
	        width = desiredWidth;
	        
	    }

	    //Measure Height
	    if (heightMode == MeasureSpec.EXACTLY) {
	        //Must be this size
	        height = heightSize;
	    } else if (heightMode == MeasureSpec.AT_MOST) {
	        //Can't be bigger than...
	        height = Math.min(desiredHeight, heightSize);
	    } else {
	        //Be whatever you want
	        height = desiredHeight;
	    }
	    //Log.i("spec",width+","+height);
	    //MUST CALL THIS	    
	    setMeasuredDimension(width, height);
	}

}