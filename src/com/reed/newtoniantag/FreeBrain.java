package com.reed.newtoniantag;

import java.io.Serializable;
import java.util.ArrayList;

import android.view.MotionEvent;

public class FreeBrain extends Holderable implements Serializable{	
	private static final long serialVersionUID = 1L;
	Bot b;
	int wait;
	static int waitTime=60;
	FreeBrain(Bot b){
		super(0,0);
		this.b=b;
	}	
}


interface MakePlayerListener{
	void onMakePlayer(Bot b);
}
class MakePlayerEvent implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ArrayList<MakePlayerListener> listeners;
	MakePlayerEvent(){
		listeners=new ArrayList<MakePlayerListener>();
	}
	void register(MakePlayerListener e){
		listeners.add(e);
	}
	void fire(Bot e){
		for(MakePlayerListener i:listeners){
			i.onMakePlayer(e);
		}
	}
	void clear(){
		listeners.clear();
	}
}
class FreeBrainManager implements Serializable{	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<FreeBrain> freebrains=new ArrayList<FreeBrain>();
	private ArrayList<FreeBrain> brainsBeingHeld=new ArrayList<FreeBrain>();
	private MakePlayerEvent event=new MakePlayerEvent();
	FreeBrain createFreeBrain(Bot b){
		FreeBrain fb=new FreeBrain(b);
		freebrains.add(fb);
		return fb;
	}
	void registerListener(MakePlayerListener e){
		event.register(e);
	}
	void step(){
		ArrayList<FreeBrain> finished=new ArrayList<FreeBrain>();
		for(FreeBrain j:this.brainsBeingHeld){
			j.wait++;
			if(j.wait>FreeBrain.waitTime){
				finished.add(j);
			}
		}
		for(FreeBrain j:finished){
			
			this.brainsBeingHeld.remove(j);
			j.wait=0;
			event.fire(j.b);
			//createPlayer(j.b,MGameS.getColor());
		}
	}
	void hold(FreeBrain fb){
		this.brainsBeingHeld.add(fb);
	}
	boolean deleteHeld(int pid){
		for(FreeBrain j:brainsBeingHeld){
			if(j.holderID()==pid){										
				brainsBeingHeld.remove(j);
				j.release();
				j.wait=0;
				return true;
			}
		}
		return false;
	}
	void deleteHeld(MotionEvent e){
		ArrayList<FreeBrain> toremove=new ArrayList<FreeBrain>();
		for(FreeBrain f:this.brainsBeingHeld){
			int id=f.holderID();
			int zap=e.findPointerIndex(id);
			if(zap==-1){
				toremove.add(f);
			}
		}
		for(FreeBrain f:toremove){
			f.release();
			f.wait=0;
			brainsBeingHeld.remove(f);
		}
	}
	FreeBrain findClosestNotBeingHeld(float xx,float yy,float diam){
		FreeBrain closest=null;			
		float clos=diam;		
		for(int ii=0;ii<freebrains.size();ii++){			
			FreeBrain i=freebrains.get(ii);
			if(!i.beingHeld()){
				Vec2 j=new Vec2(xx,yy);
            	j.sub(i.b.pos);
            	float len=(float)Math.sqrt(j.lengthSqr());
            	if(len<diam){
            		if(len<clos){
            			closest=i;
            			clos=len;
            		}	            		
            	}
			}
		}
		return closest;
	}
}
