package com.reed.newtoniantag;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

import android.graphics.Color;
import android.os.SystemClock;
import android.view.MotionEvent;

class MFPS implements Serializable {
	private static final long serialVersionUID = 1L;
	float smoothedfps = 0;
	final float smoothing = 0.875f;
	final float comp = 1 - smoothing;
	long lasttime;
	long diff;
	float newfps;

	void tick() {
		long k = System.currentTimeMillis();
		diff = k - lasttime;

		float timeToDoOneFrame = diff;
		newfps = 1.0f / timeToDoOneFrame * 1000;

		smoothedfps = smoothing * smoothedfps + comp * newfps;
		lasttime = k;
	}

	float getfps() {
		return newfps;
	}
}

class MGame implements Serializable {

	private static final long serialVersionUID = 1L;
	// Vec3 bounds=new Vec3(0,0,1.0f);

	static final int aspectRatioX = 10;
	static final int aspectRatioY = 16;
	RectFS bounds = new RectFS(0, 0, aspectRatioX, aspectRatioY);
	boolean running = false;
	// Player player;

	MFPS fps = new MFPS();
	// TagManager tagman=new TarManager();
	FreeBrainManager freebrainman = new FreeBrainManager();
	PlayerManager playerman = new PlayerManager();
	BotManager botman = new BotManager();
	TagManager tagman = new TagManager();
	TeamManager teamman = new TeamManager();
	// Player currplayer;
	private static Vec2 tempy = new Vec2();

	String debug = "";

	// ArrayList<Bot> zombies=new ArrayList<Bot>();
	// ArrayList<Player> toremove=new ArrayList<Player>();
	// ArrayList<Bot> toadd=new ArrayList<Bot>();
	// ArrayList<MotionEvent> doubleClicks=new ArrayList<MotionEvent>();

	MGame() {
		// dv=ddv;
		// this.sh=sh;
		freebrainman.registerListener(playerman);
		botman.register(teamman);
		// teamman.register(tagman);

		running = false;

		// world=new World();
		// Bot it=botman.createBot(5f,5f,TeamManager.IT);

		Vec2 j = new Vec2(1, 1);
		Vec2 lol = new Vec2(0.2f, 1);

		for (int i = 0; i < 3; i++) {
			Bot b = teamman.createIT(botman, j.x, j.y);
			freebrainman.createFreeBrain(b);
			j.add(lol);
		}
		// tagman.additBot(it);

		Bot b = teamman.createFree(botman, j.x, j.y);
		playerman.createPlayer(b, MGameS.getColor());
		// players.add(createPlayer(5f,3f,Color.BLUE));
		// players.add(createPlayer(5f,3f,Color.CYAN));

		for (int i = 0; i < 4; i++) {
			Bot ba = teamman.createFree(botman, j.x, j.y);
			freebrainman.createFreeBrain(ba);
			j.add(lol);
		}

	}

	static long lastTime;
	final static long diff = (int) (1000 / 30.0f);

	boolean locked() {
		long tt = System.currentTimeMillis();
		long j = tt - lastTime;
		if (j >= diff) {
			lastTime = tt;
			return false;
		}
		return true;
	}

	void draw() {
		botman.draw();
		// player.draw(this,c, paint);
		// ai.draw(this,c,paint);
		playerman.draw();

		tempy.set(1f, 1f);

		MGameS.drawText(Integer.toString((int)fps.getfps()), tempy, Color.BLACK);

	}

	void step() {

		playerman.arrive();

		tagman.step(teamman);
		botman.step();
		botman.constrain(bounds);
		botman.move();
		// handleEvents();
		// for(int i=0;i<processclicks.size();i++){
		// handleClick(processclicks.get(i));
		// }
		// processclicks.clear();
		this.freebrainman.step();

	}

	static class MotionEventComparator implements Comparator<MotionEvent> {

		@Override
		public int compare(MotionEvent lhs, MotionEvent rhs) {
			if (lhs.getEventTime() < rhs.getEventTime()) {
				return -1;
			}
			return 1;
		}

	}

	// void sort(ArrayList<MotionEvent> list){
	// Collections.sort(list,new MotionEventComparator());
	// }
	// synchronized void handleEvents(){
	// processclicks.addAll(clicks);
	// // for(MotionEvent e:clicks){
	// // processclicks.add(e);
	// // //this.handleClick(e);
	// // }
	// clicks.clear();
	//
	// }

	public void handleClick(MotionEvent e) {

		// debug="touch me";
		// public void onClick(MotionEvent e) {
		int pind = e.getActionIndex();
		int pid = e.getPointerId(pind);

		float rawx = e.getX(pind);
		float rawy = e.getY(pind);

		float xx = MGameS.convertX(rawx);
		float yy = MGameS.convertY(rawy);
		// debug=xx+","+yy+":"+MGameS.offx+","+MGameS.offy;
		// TODO make safer. when up doesnt happen

		int n = e.getActionMasked();
		// debug="ns="+n;

		// move stuff
		this.freebrainman.deleteHeld(e);
		playerman.moveAndKill(e);

		if (n == MotionEvent.ACTION_DOWN
				|| n == MotionEvent.ACTION_POINTER_DOWN) {
			// float diam=3.0f;
			Player closest = playerman.findClosestBeaconBeingHeld(xx, yy,
					Beacon.diam);

			if (closest != null) {
				closest.beacon.capture(pid);
			} else {
				FreeBrain closest2 = this.freebrainman.findClosestNotBeingHeld(
						xx, yy, Beacon.diam);
				if (closest2 != null) {
					closest2.capture(pid);
					this.freebrainman.hold(closest2);

				}
			}
		} else if (n == MotionEvent.ACTION_UP
				|| n == MotionEvent.ACTION_POINTER_UP) {
			playerman.releaseHeld(pid);

			// ArrayList<FreeBrain> toremove=new ArrayList<FreeBrain>();
			this.freebrainman.deleteHeld(pid);

		}
	}

	// synchronized public void onClick(MotionEvent e) {
	// clicks.add(e);
	// //addEvent(e,clicks);
	// //clicks.add(e);
	// }
	// synchronized public void addEvent(MotionEvent e,ArrayList<MotionEvent>
	// a){
	//
	//
	// }

	//
	// public void handleDoubleTap(MotionEvent e){
	// float rawx=e.getX();
	// float rawy=e.getY();
	// float xx=MGameS.convertX(rawx);///MGameS.zz;
	// float yy=MGameS.convertY(rawy);///MGameS.zz;
	// Vec2 mp=new Vec2(xx,yy);
	// Vec2 temp=new Vec2();
	// for(Bot i:bots){
	// temp.copy(i.pos);
	// temp.sub(mp);
	// float len=(float)Math.sqrt(temp.lengthSqr());
	// if(len<1.0f){
	// if(!i.controlledByPlayer){
	// createPlayer(i,MGameS.getColor());
	// return;
	// }else{
	// int targ=-1;
	// for(int j=0;j<players.size();j++){
	// if(players.get(j)==i.owner){
	// targ=j;
	// break;
	// }
	// }
	// players.remove(targ);
	// i.owner.detach();
	// return ;
	//
	// }
	// }
	// //i.pos.x=5;
	// }
	//
	//
	// }

	// synchronized public void onDoubleTap(MotionEvent e) {
	// //debug="watup";
	// int n=e.getActionMasked();
	// //debug="ns="+n;
	// if(n==MotionEvent.ACTION_DOWN){
	// addEvent(e,doubleClicks);
	// //doubleClicks.add(e);
	// }
	//
	// }

}
