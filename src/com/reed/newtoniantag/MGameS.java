package com.reed.newtoniantag;

import android.graphics.Bitmap;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
class MSprite{
	Bitmap b;
	Vec2 offset;
	MSprite(Bitmap b,float x,float y){
		this.b=b;
		offset=new Vec2(x,y);
	}
	
}



public class MGameS{
	static DrawView dv;
	static SurfaceHolder sh;
	static Canvas ca;
	private static float zz;
	private static int w;
	private static int h;
	//static boolean rotate=false;
	//static Rect gamerect;
	private static float offx=0;
	private static float offy=0;
	
	
	static Paint paint=new Paint(Paint.ANTI_ALIAS_FLAG);
	static MSprite shadow;
	static void updateFrame(int x,int y,int x1,int x2){
		if(w!=x||h!=y||offx!=x1||offy!=x2){
			w=x;
			h=y;		
			zz=w/10.0f;
			
		
			offx=x1;
			offy=x2;
		
		
			initShadow();		
			paint.setStrokeWidth(zz*0.1f);
		}
		//bpaint=new Paint(Paint.ANTI_ALIAS_FLAG);		
		//bpaint.setShader(new LinearGradient(0,0,0,h,Color.GRAY,Color.BLACK,Shader.TileMode.MIRROR));
	}
//	static void convert(Vec2 a){
//		a.div(zz);
//		
//	}
	static float convertX(float x){
		return ((x-offx)/zz);
	}
	static float convertY(float y){
		return ((y-offy)/zz);
	}
	
	static int[] colors={Color.CYAN,Color.MAGENTA,Color.RED,Color.DKGRAY};
	static int ccounter=0;
	static Vec2 temp=new Vec2();
	static int getColor(){
		int des=colors[ccounter];
		ccounter++;
		ccounter=ccounter % colors.length;
		return des;
	}
	static void initShadow(){
		int siz=(int)(zz*1.0f);
		//int siz=64;
		float blur=siz/6;
		Bitmap b = Bitmap.createBitmap(siz, siz, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		Paint m_Paint = new Paint(0);
		c.drawColor(Color.TRANSPARENT);
		m_Paint.setColor(Color.BLACK);
		
		m_Paint.setMaskFilter(new BlurMaskFilter(blur, BlurMaskFilter.Blur.OUTER));
		c.drawCircle(siz/2.0f, siz/2.0f, siz/2.0f-blur, m_Paint);
		shadow=new MSprite(b,siz/2.0f,siz/2.0f);
		
	}
	
	static void drawCircle(float x,float y,float radius,int c){
		paint.setColor(c);
		
		Vec2 aa=temp;
		aa.set(x,y);
		drawMSprite(shadow,aa);
		
		ca.drawCircle(x*zz, y*zz, radius*zz, paint);
		
	}
	static void drawOutlineCircle(float x,float y,float radius,int c){
		paint.setColor(c);
		paint.setStyle(Paint.Style.STROKE);	
		Vec2 aa=temp;
		aa.set(x,y);
		drawMSprite(shadow,aa);
		
		ca.drawCircle(x*zz, y*zz, radius*zz, paint);
		paint.setStyle(Paint.Style.FILL);
		
		
	}
	
	static void drawMSprite(MSprite s,Vec2 pos){
		Vec2 te=temp;//new Vec2(pos);
		te.copy(temp);
		te.mult(zz);
		te.sub(s.offset);
		ca.drawBitmap(s.b,te.x,te.y,null);
	}
	static void drawLine(Vec2 a,Vec2 b,int c){
		paint.setColor(c);
		ca.drawLine(a.x*zz, a.y*zz,b.x*zz, b.y*zz, paint);
	}
	static void drawLine(Vec2 a,Vec2 b,int c,int alpha){
		paint.setColor(c);
		paint.setAlpha(alpha);
		ca.drawLine(a.x*zz,a.y*zz,b.x*zz, b.y*zz, paint);
	}
	static void drawVector(Vec2 pos,Vec2 vec,int c){
		paint.setColor(c);
		Vec2 te=temp;
		te.copy(vec);//new Vec2(vec);
		te.mult(40.0f);
		te.add(pos);
		ca.drawLine(pos.x*zz,pos.y*zz,te.x*zz,te.y*zz,paint);
	}
	
	static void drawBackground(){
//		Paint bpaint=new Paint(Paint.ANTI_ALIAS_FLAG);		
//		bpaint.setShader(new LinearGradient(0,0,0,h,Color.GRAY,Color.BLACK,Shader.TileMode.MIRROR));
//		ca.drawPaint(bpaint);
//		
		ca.drawColor(Color.LTGRAY);
	}
	static void drawText(String str,Vec2 a,int c){		
		paint.setColor(c);
		paint.setTextSize(60.0f);
		ca.drawText(str, a.x*zz, a.y*zz, paint);
	}
}