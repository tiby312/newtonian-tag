package com.reed.newtoniantag;

import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.reed.newtoniantag.R;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;


class Yolo implements OnTouchListener{
	//ArrayList<MotionEvent> list;
	ArrayList<MotionEvent> clicks=new ArrayList<MotionEvent>();
	Yolo(){
		
	}
	@Override
	public boolean onTouch(View arg0, MotionEvent arg1) {
		//if(clicks.size()<2){
	
//			
//			
//		try {
//			Thread.sleep(1000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		//Log.i("jfdasfsd","j");
		synchronized(MainActivity.lock){
			clicks.add(arg1);
			while(true){
				try {
					MainActivity.lock.wait();
					break;
				} catch (InterruptedException e) {
					
				}
			}
		}
		return true;
	}	
}
public class MainActivity extends Activity implements Runnable{
	DrawView drawView;
	MGame game;
	static Thread gamethread;
	
	Yolo touchlistener;
	boolean ready=false;
	static Lock lock=new ReentrantLock();
	//ArrayList<MotionEvent> processclicks=new ArrayList<MotionEvent>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		drawView = (DrawView) findViewById(R.id.surfaceView1);

		game=SaveLoad.tryRestoreGame(savedInstanceState);
		if (game==null) {
			game = new MGame();
		}
		
		
		gamethread=new Thread(this);//need this since onWIndowFoxusCHanged may not call before pause.
		touchlistener=new Yolo();
		drawView.setOnTouchListener(touchlistener);
	}

	@Override
	public void onWindowFocusChanged(boolean a) {
		super.onWindowFocusChanged(a);
		if(a){
			
			//Log.i("welp",drawView.getWidth()+","+drawView.getHeight());
			int[] n=new int[2];
			drawView.getLocationOnScreen(n);
			MGameS.updateFrame(drawView.getWidth(),drawView.getHeight(),0,0);
			
			synchronized(this){
				if (!game.running) {
					gamethread = new Thread(this);
					gamethread.start();
					game.running=true;
				}
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();		
		synchronized(this){
			while (true) {
				try {
					game.running=false;
//					if(gamethread==null){//had error here once
//						break;
//					}
					gamethread.join();					
					break;
				} catch (InterruptedException e) {
					game.running=true;
					continue;
				}
			}
		}
	}
	@Override
	public void run() {				
		while(game.running){
			while(game.locked()){
				
			}
			game.fps.tick();			
			//Rect a=new Rect(0,0,MGameS.zz,MGameS.zz);
			MGameS.ca=MGameS.sh.lockCanvas();
						
			//if(MGameS.ca!=null){
			game.step();
			synchronized(lock){
				for(MotionEvent e:touchlistener.clicks){
					game.handleClick(e);
				}
				touchlistener.clicks.clear();
				lock.notify();
			}
			
				synchronized(MGameS.sh){					
					MGameS.drawBackground();					
					game.draw();					
				}
				MGameS.sh.unlockCanvasAndPost(MGameS.ca);
			//}
		}
		
	}


	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		SaveLoad.save(savedInstanceState, game);
	}




}
