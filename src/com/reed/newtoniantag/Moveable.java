package com.reed.newtoniantag;

import java.io.Serializable;

import android.util.FloatMath;

public class Moveable extends Target  implements Serializable{		
	private static final long serialVersionUID = 1L;
	static class Answer implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		Vec2 offset;
		float dis;
		Answer(){
			offset=new Vec2();
		}
	}
	Vec2 ang;	
	float angv;	
	final float mass;
	final float moment;
	float speed;
	static Vec2 tempy=new Vec2();
	static Vec2 tempy2=new Vec2();
	Moveable(float x,float y){
		super(x,y);
		
		
		
		ang=new Vec2(1,0);
		angv=0;
		mass=10.0f;
		moment=(mass/4.0f)*radius*radius;
		speed=0;
	}
	void step(){
		pos.add(vel);
		ang.rotate(angv);
		speed=(float)Math.sqrt(vel.lengthSqr());
	}
	
	void applyTorque(float a){
		angv+=a/moment;
		
	}
	void applyForce(Vec2 a){
		tempy.copy(a);
		
		//Vec2.tempVec2.copy(a);
		tempy.div(mass);
		//Vec2.tempVec2.div(mass);
		vel.add(tempy);
		
	}
	
	static boolean checkCollision(Moveable a,Moveable b,Answer ans){		
		Vec2 k=tempy;//new Vec2();
		k.copy(b.pos);
		k.sub(a.pos);
		if(k.lengthSqr()<0.00001f){
			ans.offset.copy(k);
			ans.dis=0.0f;
			return true;
		}
		float len=(float)Math.sqrt(k.lengthSqr());
		if(len<a.radius+b.radius){
			ans.offset=k;
			ans.dis=len;
			return true;
		}
		return false;
	}
	static void handleCollide(Moveable a,Moveable b,Answer ans){
		if(ans.dis<0.00001f){
			float xx1=-1.0f+2*(float)Math.random();
			//j^2+n^2=1
			//n^2=1-j^2
			//n=sqrt(1-j^2);
			float xx2=(float)Math.sqrt(1-FloatMath.pow(xx1, 2));
			tempy.set(xx1,xx2);
			//Vec2 temp=new Vec2(xx1,xx2);
			tempy.mult(0.1f);
			b.applyForce(tempy);
			tempy.neg();
			a.applyForce(tempy);
			return;
		}
        float t=a.radius+b.radius;
        float intensity=1.0f-(ans.dis/t);
        intensity*=intensity;
        //intensity*=40.0f;
        //MGameS.drawLine(a.pos,b.pos, Color.GREEN);
        Vec2 norm=tempy;//new Vec2();
        norm.copy(ans.offset);
        norm.normalize();
        Vec2 temp2=tempy2;//new Vec2();
        temp2.copy(norm);
        temp2.mult(intensity);
        b.applyForce(temp2);
        temp2.neg();
        a.applyForce(temp2);
	}
}
