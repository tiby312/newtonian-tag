package com.reed.newtoniantag;

import java.io.Serializable;

import android.graphics.Color;

public class Player implements Serializable{	
	private static final long serialVersionUID = 1L;
	Beacon beacon;
	Bot b;
	int playercol;
	Player(Bot bo, int color) {
		//super(x, y, color);
		b=bo;
		beacon=new Beacon(b.pos.x,b.pos.y);
		
		b.controlledByPlayer=true;
		//b.target=beacon;
		//b.team=color;
		playercol=color;
		b.owner=this;
	}
	void draw(){
		MGameS.drawCircle(beacon.pos.x, beacon.pos.y, Beacon.radius, playercol);
		
		MGameS.drawOutlineCircle(beacon.pos.x, beacon.pos.y, Beacon.diam, playercol);
		MGameS.drawOutlineCircle(b.pos.x, b.pos.y, b.radius, playercol);
		//MGameS.drawLine(pos, beacon.pos, color);
		if(beacon.beingHeld()){
			MGameS.drawCircle(b.pos.x, b.pos.y, Beacon.radius, Color.BLACK);	
		}
	}
	void detach(){
		playercol=Color.GRAY;
		b.controlledByPlayer=false;
		b.owner=null;
		b=null;		
		beacon=null;
	}

}
