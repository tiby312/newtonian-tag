package com.reed.newtoniantag;

import java.io.Serializable;
import java.util.ArrayList;

import android.view.MotionEvent;

public class PlayerManager implements MakePlayerListener,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Player> players=new ArrayList<Player>();
	private static Vec2 temp=new Vec2();
	@Override
	public void onMakePlayer(Bot b) {
		// TODO Auto-generated method stub
		createPlayer(b,MGameS.getColor());
	}
	Player createPlayer(Bot b,int col){
		Player j=new Player(b,col);		
		players.add(j);
		return j;
	}
	void draw(){
		for(int i=0;i<players.size();i++){
			players.get(i).draw();
		}		
	}
	boolean releaseHeld(int pid){
		for(Player i:players){
    		if(i.beacon.holderID()==pid){
    			i.beacon.release();
    			return true;
    		}
    	}
		return false;
	}
	void moveAndKill(MotionEvent e){
		for(int jj=0;jj<players.size();jj++){			
			Player i=players.get(jj);
    		if(i.beacon.beingHeld()){
    			int id=i.beacon.holderID();
    			
    			int zap=e.findPointerIndex(id);
    			if(zap==-1){
    				i.beacon.release();
    			}else{	    				
	    			float rawxxx=e.getX(zap);
	    			float rawyyy=e.getY(zap);
	    			
	    			float xxx=MGameS.convertX(rawxxx);///(float)MGameS.zz;
	    			float yyy=MGameS.convertY(rawyyy);///(float)MGameS.zz;
	    			i.beacon.setTarget(xxx, yyy);
    			}
    		}
    	}
	}
	Player findClosestBeaconBeingHeld(float xx,float yy,float diam){
		Player closest=null;			
		float clos=diam;		
		for(int jj=0;jj<players.size();jj++){
			Player i=players.get(jj);		
    		if(!i.beacon.beingHeld()){
        		Vec2 j=temp;
    			j.set(xx,yy);
            	j.sub(i.beacon.pos);
            	float len=(float)Math.sqrt(j.lengthSqr());
            	if(len<diam){
            		if(len<clos){
            			closest=i;
            			clos=len;
            		}	            		
            	}
    		}
    	}
		return closest;
	}
	void arrive(){		
		for(int jj=0;jj<players.size();jj++){
			Player i=players.get(jj);
		
			//i.beacon.step();
			i.b.arrive(i.beacon);
			//i.b.arrive(i.b.timeToRot(),ans,i.beacon);
			//i.b.desiredSteer.add(ans);
		}	   
		
	}
}
