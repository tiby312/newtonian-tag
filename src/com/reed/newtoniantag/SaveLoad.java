package com.reed.newtoniantag;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import android.os.Bundle;

public class SaveLoad {
	static void save(Bundle savedInstanceState,MGame game){
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(game);
			byte[] yourBytes = bos.toByteArray();
			savedInstanceState.putByteArray("gamestate", yourBytes);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (out != null) {
					out.close();
				}
			} catch (IOException ex) {
				// ignore close exception
			}
			try {
				bos.close();
			} catch (IOException ex) {
				// ignore close exception
			}
		}

	}
	static MGame tryRestoreGame(Bundle savedInstanceState) {
		if (savedInstanceState == null) {
			return null;
		}
		if (savedInstanceState.containsKey("gamestate")) {
			byte[] a = savedInstanceState.getByteArray("gamestate");
			ByteArrayInputStream bis = new ByteArrayInputStream(a);
			ObjectInput in = null;
			try {
				in = new ObjectInputStream(bis);
				Object o = in.readObject();				
				return (MGame) o;
			} catch (StreamCorruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					bis.close();
				} catch (IOException ex) {
					// ignore close exception
				}
				try {
					if (in != null) {
						in.close();
					}
				} catch (IOException ex) {
				}
			}
		}
		return null;
	}

}
