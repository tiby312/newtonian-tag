package com.reed.newtoniantag;

import java.io.Serializable;

import android.util.FloatMath;

public class Ship extends Moveable implements Serializable{	
	private static final long serialVersionUID = 1L;
	final float forwardPower;
	final float turnPower;
	
	final float turnacc;
	final float linearacc;
	final float rottime;
	int goingleft=0;
	int goingright=0;
	int goingforward=0;
	static int inc=20;
	static int decay=8;
	static int max=255-inc-1;
	static private Vec2 temp=new Vec2();
	Ship(float x,float y){
		super(x,y);
		forwardPower=0.03f;
		turnPower=0.0008f;
		
		turnacc=turnPower/moment;
		linearacc=forwardPower/mass;
		rottime=(float)Math.sqrt((Tools.fourthpi)/linearacc);
	}
	void step(){
		super.step();
		if(goingforward>=inc){
			goingforward-=decay;
		}
		if(goingleft>=inc){
			goingleft-=decay;
		}
		if(goingright>=inc){
			goingright-=decay;
		}
	}
	void goforward(){
		
		temp.copy(ang);
		//Vec2.tempVec.copy(ang);
		//Vec2.tempVec.mult(forwardPower);
		temp.mult(forwardPower);
		this.applyForce(temp);
		if(goingforward<max){
			goingforward+=inc;
		}
	}
	void goleft(){
		applyTorque(turnPower);
		if(goingleft<max){
			goingleft+=inc;
		}
	}
	void goright(){
		applyTorque(-turnPower);
		if(goingright<max){
			goingright+=inc;
		}
	}

	
}
