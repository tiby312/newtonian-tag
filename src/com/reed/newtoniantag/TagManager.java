package com.reed.newtoniantag;

import java.io.Serializable;
import java.util.ArrayList;

import android.graphics.Color;

//interface SwitchTeamListener{
//	void onSwitchTeam(Bot a,Bot b);
//}
//class SwitchTeamEvent{
//	ArrayList<SwitchTeamListener> listeners;
//	SwitchTeamEvent(){
//		listeners=new ArrayList<SwitchTeamListener>();
//	}
//	void register(SwitchTeamListener e){
//		listeners.add(e);
//	}
//	void fire(Bot a,Bot b){
//		for(SwitchTeamListener i:listeners){
//			i.onSwitchTeam(a,b);
//		}
//	}
//	void clear(){
//		listeners.clear();
//	}
//}
class TeamManager implements CollisionListener,Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final int IT=Color.BLUE;
	static final int FREE=Color.GREEN;
	
	ArrayList<Bot> itteam=new ArrayList<Bot>();
	ArrayList<Bot> freeteam=new ArrayList<Bot>();
	//private SwitchTeamEvent event=new SwitchTeamEvent();
	
	
	Bot createFree(BotManager b,float x,float y){
		Bot a=b.createBot(x, y, FREE);
		freeteam.add(a);
		return a;
	}
	Bot createIT(BotManager b,float x,float y){
		Bot a= b.createBot(x, y, IT);
		itteam.add(a);
		return a;
	}
	
//	void register(SwitchTeamListener e){
//		event.register(e);
//	}
	void attemptSwitchTeams(Bot a,Bot b){
		if(a.teamcooldown<=0&&b.teamcooldown<=0&&a.team!=b.team){
			//event.fire(a,b);
			trySwitch(a);
			trySwitch(b);
			a.teamcooldown=30;
			b.teamcooldown=30;
			int temp=a.team;
			a.team=b.team;
			b.team=temp;
			
		}
	
	}
	void trySwitch(Bot a){		
		if(a.team==TeamManager.IT){
			this.freeteam.add(a);
			this.itteam.remove(a);
		}else if(a.team==TeamManager.FREE){
			this.freeteam.remove(a);
			this.itteam.add(a);
		}				
	}
	@Override
	public void onCollision(Bot a, Bot b) {
		attemptSwitchTeams(a,b);		
	}
}


public class TagManager implements Serializable{
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//	private ArrayList<Bot> freebots=new ArrayList<Bot>();
//	private ArrayList<Bot> itbots=new ArrayList<Bot>();
//	
//	void addfreeBot(Bot b){		
//		freebots.add(b);		
//	}
//	void additBot(Bot b){
//		itbots.add(b);
//	}
	Bot.AvoidAnswer avans=new Bot.AvoidAnswer();
	Bot.AvoidAnswer sepans=new Bot.AvoidAnswer();
	void step(TeamManager ta){
	
		for(int i=0;i<ta.freeteam.size();i++){
			Bot aa=ta.freeteam.get(i);
			if(aa.owner==null){
				for(int j=0;j<ta.itteam.size();j++){				
					Bot bb=ta.itteam.get(j);
					if(Bot.avoid(aa, bb, avans)){									
						aa.desiredSteer.add(avans.v1);										
					}
					if(Bot.separate(aa, bb, sepans)){									
						aa.desiredSteer.add(sepans.v1);										
					}
				}
				for(int j=i+1;j<ta.freeteam.size();j++){									
					Bot bb=ta.freeteam.get(j);
					if(Bot.avoid(aa, bb, avans)){									
						aa.desiredSteer.add(avans.v1);
						bb.desiredSteer.add(avans.v2);					
					}
				}
			}
		}
//		for(int i=0;i<ta.freeteam.size();i++){
//			Bot aa=ta.freeteam.get(i);
//			if(aa.owner==null){ //hack way to iterate over freebots;
//				
//			}
//		}
		for(int i=0;i<ta.itteam.size();i++){
			Bot aa=ta.itteam.get(i);
			if(aa.owner==null){
				for(int j=i+1;j<ta.itteam.size();j++){									
					Bot bb=ta.itteam.get(j);
					if(Bot.avoid(aa, bb, avans)){									
						aa.desiredSteer.add(avans.v1);
						bb.desiredSteer.add(avans.v2);					
					}
				}
				Bot b=findNearByFree(aa,ta.freeteam);
				if(b!=null){
					aa.arrive(b);
				}
			}
		}
//		for(int i=0;i<ta.itteam.size();i++){
//			Bot a=ta.itteam.get(i);
//			if(a.owner==null){				
//		
//			}
//		}
	}
	Bot findNearByFree(Bot bo,ArrayList<Bot> list){
		Bot closest=null;
		float clos=10.0f;
		for(int i=0;i<list.size();i++){
			Bot a=list.get(i);
			float dis=Vec2.dis(bo.pos,a.pos);
			if(dis<clos){
				closest=a;
				clos=dis;
			}
		}
		return closest;
	}
//	void trySwitch(Bot a){
//		if(a.owner==null){
//			if(a.team==TeamManager.IT){
//				this.freebots.add(a);
//				this.itbots.remove(a);
//			}else if(a.team==TeamManager.FREE){
//				this.freebots.remove(a);
//				this.itbots.add(a);
//			}
//		}
//		
//	}
//	@Override
//	public void onSwitchTeam(Bot a, Bot b) {
//		trySwitch(a);
//		trySwitch(b);
//		
//	}
}
