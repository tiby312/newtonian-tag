package com.reed.newtoniantag;


public class Tools{
	static final float halfPI=(float)Math.PI/2.0f;
	static final float fourthpi=(float)Math.PI/4.0f;
	static final float eighthpi=(float)Math.PI/8.0f;
	
	
	static boolean contains(Circle c,RectFS bound){
		if(c.pos.x-c.radius>=bound.left&&
				
				c.pos.y-c.radius>=bound.top&&
				c.pos.x+c.radius<bound.right&&
				c.pos.y+c.radius<bound.bottom){
			return true;
		}
		return false;
	}
	static float fastacos(float x) {
	    //Num a-
	   //return (-0.69813170079773212 * x * x - 0.87266462599716477) * x + 1.5707963267948966;
	   float a=-0.69813170079773212f;
	   float b=- 0.87266462599716477f;
	   float c=1.5707963267948966f;
	   return (a* x * x +b) * x + c;
	}
	static float sign(float a){
		if(a>0){
			return 1.0f;
		}
		if(a<0){
			return -1.0f;
		}
		return 0f;
	}
//	static float pi=(float)Math.PI;
//	static float twopi=(float)(Math.PI*Math.PI);
//	static float cos(float x){
//	    //x = fmod(x + Z_PI, Z_PI * 2) - Z_PI; // restrict x so that -M_PI < x < M_PI
//	    float B = 4.0f/pi;
//	    float C = -4.0f/twopi;
//
//	    float y = B * x + C * x * Math.abs(x);
//
//	    float P = 0.225f;
//
//	    return P * (y * Math.abs(y) - y) + y;		
//	}
//	static float sin(float x){
//	    return sin(x + (pi / 2));
//	}

	
}

