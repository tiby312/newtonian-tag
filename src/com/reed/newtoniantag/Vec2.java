package com.reed.newtoniantag;

import java.io.Serializable;


class RectFS implements Serializable{	
	private static final long serialVersionUID = 1L;
	float top;
	float left;
	float bottom;
	float right;
	RectFS(float left,float top,float right,float bottom){
		this.left=left;
		this.top=top;
		this.right=right;
		this.bottom=bottom;
	}
}

//
//class Vec3 implements Serializable{
//	
//	private static final long serialVersionUID = 1L;
//	float x;
//	float y;
//	float z;
//	Vec3(float x,float y,float z){
//		this.x=x;
//		this.y=y;
//		this.z=z;
//	}
//}
public class Vec2 implements Serializable{

	private static final long serialVersionUID = 1L;
	float x;
	float y;	
	private static Vec2 tempVec=new Vec2();
	private static Vec2 tempVec2=new Vec2();
	Vec2(){
		x=0;
		y=0;
	}
	Vec2(Vec2 o){
		this.x=o.x;
		this.y=o.y;
	}	
	Vec2(float x,float y){
		this.x=x;
		this.y=y;
	}
	void set(float x,float y){
		this.x=x;
		this.y=y;
	}
	static float dis(Vec2 a,Vec2 b){
		tempVec.copy(a);
		tempVec.sub(b);
		return (float)Math.sqrt(tempVec.lengthSqr());
	}
	void perp90deg(){
		float xx=x;
		float yy=y;
		x=-yy;
		y=xx;		
	}
	void copy(Vec2 a){
		x=a.x;
		y=a.y;
	}
	void rotate(float theta){ //only work for unit vector
		tempVec.x=(float)Math.cos(theta);
		tempVec.y=(float)Math.sin(theta);
		//tempVec.normalize();	    
	    rotate(tempVec);
	    normalize();
	}
	void normalize(){
		float l=(float)Math.sqrt(lengthSqr());
		x=x/l;
		y=y/l;
	}
	float lengthSqr(){
		return x*x+y*y;
	}
	void rotate(Vec2 a){
		tempVec2.copy(this);
        x = tempVec2.x * a.x - tempVec2.y * a.y;
        y = tempVec2.x * a.y+ tempVec2.y * a.x;
	}
    static float innerProduct( Vec2 a,Vec2 b){
        return (a.x*b.x)+(a.y*b.y);
    }
    static float crossProduct(Vec2 a,Vec2 b){
        return (a.x*b.y)-(a.y*b.x);
    }
    static float innerAngle(float innerProduct){
        return Tools.fastacos(innerProduct);
    }

    void truncate(float j){
    	normalize();
    	mult(j);
    }
    void neg(){
    	x=-x;
    	y=-y;
    }
    void add(Vec2 a){
    	x+=a.x;
    	y+=a.y;
    }
    void sub(Vec2 a){
    	x-=a.x;
    	y-=a.y;
    }
    void mult(float a){
    	x*=a;
    	y*=a;
    }
	void div(float a){
		x/=a;
		y/=a;
	}
	public String toString(){
		return "("+x+","+y+")";
	}

}

